import urllib.request
import os
import zipfile
import shutil

import const
import mongodb_storage

import os

image_directory = 'images'

if not os.path.exists(image_directory):
    os.makedirs(image_directory)


def download_image(link, linkedin_id):
    print('start to download', linkedin_id)
    filename = f"{image_directory}/{linkedin_id}.jpg"
    urllib.request.urlretrieve(link, filename)
    return filename


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file),
                       os.path.relpath(os.path.join(root, file),
                                       os.path.join(path, '..')))


mongo_instance = mongodb_storage.MongoDBStorage()
valid_record = list(
    mongo_instance.git_collection.aggregate([{'$match': {'results.version': const.VERSION,
                                                         'results.data.about.logo':{'$ne':None}}}, {'$project': {
        'results': {'$filter': {
            'input': '$results',
            'as': 'item',
            'cond': {'$and': [{'$eq': ['$$item.version', const.VERSION]},
                              {'$eq': ['$$item.error', False]}]}
        }}
    }},
                                             {'$match': {'results.0': {'$exists': True}}}]))
for record in valid_record:
    last_result = record['results'][-1]
    result_logo = last_result['data']['about']['logo']
    result_linkedin_id = last_result['data']['about']['linkedinID']
    download_image(result_logo, result_linkedin_id)

zipf = zipfile.ZipFile('images.zip', 'w', zipfile.ZIP_DEFLATED)
zipdir('images', zipf)
zipf.close()
