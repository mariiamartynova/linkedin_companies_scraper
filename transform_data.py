import const
import mongodb_storage
import pandas as pd

mongo_instance = mongodb_storage.MongoDBStorage()
all_companies = list(
    # mongo_instance.git_collection.find(
    # mongo_instance.git_collection.find({'results.error': False, 'results.data.about.SourceUrl': {'$exists': True}},
    # mongo_instance.git_collection.find({'results.version': const.VERSION, 'results.error': {'$ne': True}}, # main run
    # mongo_instance.git_collection.find({'_id': 'circus-marketing'},
    mongo_instance.git_collection.find({'results.error': True},
    # mongo_instance.git_collection.find({'results.version': 1.3},
                                       {'results.data.pages': 0}))
                                       # {'results.data.pages': 0}).limit(101))
print('b')


# TODO filter results.error': False in function
# all_data = all_data[:9]

def get_company_size(size_dict):
    start_count = size_dict['start']
    company_size = str()
    if 'start' in size_dict and 'end' in size_dict:
        company_size = f'{start_count}-{size_dict["end"]}'
    elif 'start' in size_dict and 'end' not in size_dict:
        if start_count != 0:
            company_size = f'{start_count}+'
        else:
            company_size = start_count
    return company_size


def get_main_info(all_data):
    df_data = []
    affiliated_list = []
    for db_company in all_data:
        company = list(filter(lambda res: res['error'] == False, db_company['results']))
        if not company:
            continue
        else:
            company = company[-1]['data']
        company_data = {}
        # company_data['Source'] = company['Source']
        # company_data.update(company)
        # df_data.append(company_data)
        # affiliated_dict = {}
        # affiliated_dict['LinkedinID'] = None
        # affiliated_dict['Source'] = company_data['Source']
        # affiliated_dict['Affiliated Page'] = None
        # affiliated_dict['Type'] = None
        # affiliated_list.append(affiliated_dict)
        try:
            company_data['Source'] = company['about']['Source']
        except:
            company_data['Source'] = company['Source']
        company_data['LinkedInURL'] = company['about']['LinkedInURL']
        try:
            company_data['VisitWebsiteButton'] = company['about']['SourceUrl']
        except KeyError:
            company_data['VisitWebsiteButton'] = None
        company_data['Name'] = company['about']['Name']
        company_address = company['about']['companyAddress']
        company_data['companyAddress'] = None if company_address is None else ', '.join(
            # [company_address.get('line1', ''), company_address.get('city', ''),
            [company_address.get('city', ''),
             company_address.get('postalCode', ''), company_address.get('geographicArea', ''),
             company_address.get('country', '')]).strip(', ')
        company_data['companySize'] = None if company['about']['companySize'] is None else get_company_size(
            company['about']['companySize'])
        # company_data['companyUrl'] = company_data['Source']
        company_data['description'] = company['about']['description']
        try:
            company_data.update(company['voyager'])
        except KeyError:
            company_data['distribution'] = None
        # company_data.update(company['voyager']) _______________________ NEW WERSION ________________
        # company_data.update(company['people'])
        company_data['employeesOnLinkedIn'] = company['about']['employeesOnLinkedIn']
        company_data['followerCount'] = company['about']['followerCount']
        company_data['founded'] = company['about']['founded']
        company_data['headquarters'] = company['about']['headquarters']
        try:
            company_data['industry'] = company['about']['industry']
        except KeyError:
            company_data['industry'] = None
        company_data['LinkedinID'] = company['about']['linkedinID']
        try:
            company_data['OthersID'] = ', '.join(company['about']['OthersID'])
        except (TypeError, KeyError):
            company_data['OthersID'] = None
        # company_data['linkedinID'] = company['about']['linkedinID'] if company['about']['linkedinID'] else company['about']['LinkedInMainCompanyId']
        company_data['logo'] = company['about']['logo']
        # company_data['LinkedInMainCompanyId'] = company['about']['LinkedInMainCompanyId']
        company_data['specialties'] = company['about']['specialties']
        company_data['totalEmployeeCount'] = company['about']['totalEmployeeCount']
        company_data['type'] = company['about']['type']
        company_data['location'] = company['about']['location']
        try:
            filtered_affilated = dict(list(company['affilated_pages']['companies'].items()))
                # dict(list(
                # filter(lambda elem: elem[1]['parentship'] != 'Parent',
                #        company['affilated_pages']['companies'].items())))
        except (TypeError, KeyError):
            filtered_affilated = None
        if filtered_affilated:
            if len(filtered_affilated) == 0:
                company_data['Has showcase?'] = 'n'
            for k, v in filtered_affilated.items():
                if v['parentship'] == 'Showcase page':
                    company_data['Has showcase?'] = 'y'
                    break
                else:
                    company_data['Has showcase?'] = 'n'
        unavailable_title = 'This LinkedIn Page isn’t available'
        if not filtered_affilated and company_data['LinkedInURL'] != unavailable_title:
            company_data['Has showcase?'] = 'n'
        df_data.append(company_data)
        try:
            filtered_no_showcase = dict(list(
                    filter(lambda elem: elem[1]['parentship'] != 'Showcase page',
                           company['affilated_pages']['companies'].items())))
        except KeyError:
            filtered_no_showcase = None
        if filtered_no_showcase or filtered_affilated:
            for no_showcase_key, no_showcase_value in filtered_no_showcase.items():
                affiliated_dict = {}
                affiliated_dict['LinkedinID'] = company_data['LinkedinID']
                affiliated_dict['Source'] = company_data['Source']
                affiliated_dict['Affiliated Page'] = no_showcase_key
                affiliated_dict['Type'] = no_showcase_value['parentship']
                affiliated_list.append(affiliated_dict)
                print('company' + affiliated_dict['LinkedinID'])
        else:
            affiliated_dict = {}
            affiliated_dict['LinkedinID'] = company_data['LinkedinID']
            affiliated_dict['Source'] = company_data['Source']
            affiliated_dict['Affiliated Page'] = 'This LinkedIn Page isn’t available'
            affiliated_dict['Type'] = None
            affiliated_list.append(affiliated_dict)
    affiliated = pd.DataFrame(affiliated_list)
    affiliated.to_csv('linkedin_error_affiliated_result_companies.csv', index=False)
    df = pd.DataFrame(df_data)
    df.to_csv('linkedin_error_main_result_companies.csv', index=False)


get_main_info(all_companies)
