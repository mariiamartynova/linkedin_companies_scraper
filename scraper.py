import argparse
import asyncio
import html
import json
import logging
import random
import re
import time
import traceback
from urllib import parse
from urllib.parse import unquote

import nest_asyncio
import pandas as pd
import requests
from bs4 import BeautifulSoup
from parsel import Selector
from requests import Response

import const
import mongodb_storage
from account_rotator.login_manager import LoginManager
from const import USE_CACHE
from entities import Action, ActionType, Page, CompanyData, CompanyAddress, CompanySize, \
    AffiliatedPage

# from pypeter_test import LoginManager

nest_asyncio.apply()
start_time = time.time()

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:91.0) Gecko/20100101 Firefox/91.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US;q=0.8,en;q=0.7',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-User': '?1',
    'TE': 'trailers'
}

run_id = random.randint(0, 999999999)
run_timestamp = int(start_time)
mongo_instance = mongodb_storage.MongoDBStorage()

parser = argparse.ArgumentParser(description='Linkedin parser')
parser.add_argument('block_n', type=int)

class InvalidCompany(Exception):
    pass


class UnavailablePage(Exception):
    pass

class AccountsBanned(Exception):
    pass


class LinkedinParser:
    driver = None
    #session = None
    domain = 'https://www.linkedin.com'
    retries = 5
    driver_requests = 0
    session_setuped = False
    login_data = None

    def __init__(self, company_names):
        logging.basicConfig(
            format='%(asctime)s | %(name)s | %(levelname)s | %(message)s',
            level='INFO',
            datefmt='%Y-%m-%d%H:%M:%S'
        )
        self.logger = logging.getLogger('[LINKEDIN_SCRAPER]')
        self.logger.setLevel('INFO')
        self.company_names = company_names
        #self._refresh_request_session()
        self.actions = []
        self.login_manager = LoginManager()
        # self.accounts_df = self.get_account_df()
        # self.proxies_df = self.get_proxies_df()
        self.test_attributes = []
        self.account = None
        self.session = None
        self.setup_accounts()

    def setup_accounts(self):
        self.account, cookies = self.login()
        session = requests.session()

        for cookie in cookies:
            session.cookies.set(**cookie)

        self.session = session

    def get_account_df(self):
        accounts_df = pd.read_excel(f'accounts/accounts_{str(self.block_n)}.xlsx')
        accounts_df['is_banned'].replace({"ИСТИНА": True, 'ЛОЖЬ': False}, inplace=True)
        return accounts_df

    def get_proxies_df(self):
        proxies_df = pd.read_excel('proxies.xlsx')
        proxies_df['is_banned'].replace({"ИСТИНА": True, 'ЛОЖЬ': False}, inplace=True)
        return proxies_df

    @property
    def get_parser_functions(self) -> dict:
        parser_functions = {'about': self._parse_company_about_html,
                            'voyager': self._parse_ids_html,
                            'affilated_pages': self._parse_affilated_pages_json,
                            'people': self._parse_people_html}
        return parser_functions

    def _add_action(self, action_type: ActionType, action_time: float, result=None):
        """

        :param action_time: timestamp
        :return:
        """
        self.actions.append(Action(action_type, action_time, result))

    def _get_driver_html(self):
        html_ = self.driver.find_element_by_xpath("//*").get_attribute("outerHTML")
        return html_

    def get_all_locations(self, location_list: list):
        location_full_list = []
        for location_dict in location_list:
            new_location_dict = dict()
            try:
                new_location_dict['description'] = location_dict['description']
            except KeyError:
                pass
            try:
                new_location_dict['line1'] = location_dict['line1']
            except KeyError:
                pass
            try:
                new_location_dict['city'] = location_dict['city']
            except KeyError:
                pass
            try:
                new_location_dict['geographicArea'] = location_dict['geographicArea']
            except KeyError:
                pass
            try:
                new_location_dict['postalCode'] = location_dict['postalCode']
            except KeyError:
                pass
            try:
                new_location_dict['country'] = location_dict['country']
            except KeyError:
                pass
            one_location = ', '.join(list(new_location_dict.values()))
            location_full_list.append(one_location)
        location_str = '; '.join(location_full_list)
        return location_str

    def find_logo(self, html_text, business_name, code_tag):
        logo_url = str()
        # business_name = 'Aberdeen Standard Investments'  # здесь я захардкодил, тебе нужно каждый раз передавать актуальное имя бизнеса
        best_code = json.loads(
            BeautifulSoup(html_text, 'html.parser').find('code', text=re.compile('.*company-logo.*')).text)
        logo_list_as_string = re.findall(fr'included":\s?(\[.+{business_name}.+])', html.unescape(html_text))[0]
        logo_list = json.loads(logo_list_as_string)

        for entry_dict in logo_list:
            if entry_dict.get('name') == business_name:
                first_part = entry_dict['logo']['image']['rootUrl']
                second_part = entry_dict['logo']['image']['artifacts'][-1]['fileIdentifyingUrlPathSegment']
                logo_url = ''.join([first_part, second_part])
        return logo_url

    def get_company_dict(self, page_response, business_name, company_id):
        page_response = html.unescape(page_response)
        replaced_business_name = re.escape(business_name)  # self._replace_spec_chars(business_name)
        # replaced_business_name = business_name.replace('\\', '\\\\').replace('"', '\\\\"').strip()
        # replaced_business_name = business_name.replace('"', '\\"').strip()
        regex = '\{"data":.*normalized_company:' + company_id + '".*name":"' + replaced_business_name + '.*\}'
        company_list = [json.loads(re.search(regex, page_response).group(0))]
        if len(company_list) == 1:
            company_dict = company_list[0]
        else:
            raise Exception('no such id in the list')
        # for company_dict in company_list:
        #     if company_id in company_dict:
        return company_dict
        # raise Exception('no such id in the list')

    def get_follower_count(self, page_response, business_name, company_id):
        company_dict = self.get_company_dict(page_response, business_name, company_id)
        follower_count = re.findall('[0-9]+', ''.join(
            re.findall(fr'urn:li:fs_followingInfo:urn:li:company:{company_id}(.*?)\'followingCount',
                       str(company_dict["included"]))))[0]

        return follower_count

    def get_industry(self, page_response, business_name, company_id):
        industry_mark = None
        industry = None
        company_dict = self.get_company_dict(page_response, business_name, company_id)
        for value in company_dict['included']:
            if value.get('*companyIndustries') and value.get('name') == business_name:
                try:
                    industry_mark = value['*companyIndustries'][0]
                except Exception:
                    industry_mark = None

        if industry_mark:
            for value in company_dict['included']:
                if value.get('entityUrn') and value.get('entityUrn') == industry_mark:
                    try:
                        industry = value['localizedName']
                    except Exception:
                        industry = None
        return industry

    def get_about_attributes(self, soup, page_url):
        # ToDo implement attbiutes
        tags = []
        code_tags = soup.find_all('code')
        for code_tag in code_tags:
            try:
                content = json.loads(code_tag.string)
                attrs = code_tag.attrs
                people_obj_elem = list(filter(lambda elem: elem.get('url') == page_url,
                                              content['included']))
                is_valid = True if people_obj_elem else False

                tags.append({'content': content, 'attrs': attrs, 'is_valid': is_valid, 'company': self.active_company})

            except:
                pass
        return tags

    def detect_unavailable_page(self, page_response):
        html_text = html.unescape(page_response)
        # unavailable_page = re.findall(r'{"data":{"status":404}', html_text)
        unavailable_page = re.findall(r'{"data":{"status":404}', str(html_text))
        return unavailable_page

    def _parse_company_about_html(self, page_response, previous_data):
        self.logger.info('START LONG BLOCK')
        html_text = page_response.text
        unavailable_page = self.detect_unavailable_page(page_response.text)
        if unavailable_page:
            self.logger.info('Page is unavailable')
            raise UnavailablePage
        soup = BeautifulSoup(html_text, 'html.parser')

        try:
            page_url = re.findall('com.linkedin.voyager.deco.organization.web.WebFullCompanyMain"],"url":"(.*?)"',
                                  html.unescape(page_response.text))[0]
        except IndexError:
            raise Exception
            pass

        try:
            self.test_attributes.extend(self.get_about_attributes(soup, page_url))
        except UnboundLocalError:
            pass
        # TODO fix index
        company_main_info = None
        people_obj = None
        start = time.time()
        code_tags = re.findall(
            fr'{{"data":.*normalized_company:.+"url":"{page_url}".+}}',
            html.unescape(html_text),
        )

        for tag in code_tags:
            company_main_info = json.loads(tag)

            try:
                for included_instance in company_main_info.get('included', list()):
                    if included_instance.get('url') in [page_url, unquote(page_url)]:
                        people_obj = included_instance
            except:
                pass

        end = time.time()
        self.logger.info(f'code_tags - {end - start}')

        if not people_obj:
            self.logger.info('USING MARIA\'S METHOD')
            start = time.time()
            code_tags = soup.find_all('code', text=re.compile('.*urn:li:company:.*'))
            end = time.time()
            self.logger.info(f'code_tags - {end - start}')

            for code_tag in code_tags:
                company_main_info = json.loads(code_tag.text)
                try:
                    people_obj = \
                        list(filter(lambda elem: elem.get('url') == page_url or elem.get('url') == unquote(page_url),
                                    company_main_info['included']))[0]
                    if people_obj and len(people_obj) >= 15:
                        break
                except:
                    pass

        end = time.time()
        self.logger.info(f'people_obj - {end - start}')
        start = time.time()
        try:
            jobs_url = people_obj['jobSearchPageUrl']
        except Exception:
            self.logger.info('USING MARIA\'S METHOD')
            start = time.time()
            code_tags = soup.find_all('code', text=re.compile('.*urn:li:company:.*'))
            end = time.time()
            self.logger.info(f'code_tags - {end - start}')

            for code_tag in code_tags:
                company_main_info = json.loads(code_tag.text)
                try:
                    people_obj = \
                        list(filter(lambda elem: elem.get('url') == page_url or elem.get('url') == unquote(page_url),
                                    company_main_info['included']))[0]
                    if people_obj and len(people_obj) >= 15:
                        break
                except:
                    pass

            jobs_url = people_obj['jobSearchPageUrl']

        if not people_obj:
            raise InvalidCompany

        parsed_url = parse.urlparse(jobs_url)
        url_params = parse.parse_qs(parsed_url.query)
        company_ids = url_params['f_C'][0].split(',')
        # TODO remove
        company_name = previous_data.name
        try:
            address_obj = people_obj['confirmedLocations'][0]
        except (AttributeError, IndexError):
            address_obj = {}
            self.logger.info(f'{company_name} has no address')
        company_size_obj = people_obj['staffCountRange'] or {}
        founded_on_tag = people_obj.get('foundedOn', {}) or {}
        company_type = people_obj.get('companyType', {}) or {}
        location_list = people_obj['confirmedLocations']
        employees_on_linkedin = people_obj['staffCount']

        linkedin_url = people_obj['url']
        linkedin_id = company_ids[0]
        others_id = re.findall('[0-9]+', str(
            people_obj.get('affiliatedCompaniesWithEmployeesRollupResolutionResults', {}).values()))
        name = people_obj['name']

        company_address = CompanyAddress(company_name,
                                         address_obj.get('county'),
                                         address_obj.get('geographicArea'),
                                         address_obj.get('city'),
                                         address_obj.get('postalCode'),
                                         address_obj.get('description'),
                                         address_obj.get('streetAddressOptOut'),
                                         address_obj.get('headquarter'),
                                         address_obj.get('line1'),
                                         )
        company_size = CompanySize(name, company_size_obj.get('start'), company_size_obj.get('end'))

        source_url = people_obj.get('companyPageUrl')
        description = people_obj['description']
        try:
            follower_count = [i for i in company_main_info['included'] if
                              i.get('followerCount') and i['entityUrn'].endswith(
                                  linkedin_id)][0]['followerCount']
        except IndexError:
            follower_count = self.get_follower_count(page_response.text, name, linkedin_id)
        except:
            # ToDo specify exception
            follower_count = None
        founded = founded_on_tag.get('year')

        try:
            headquarters = ', '.join([people_obj['headquarter']['city'],
                                      people_obj['headquarter']['geographicArea']])
        except (KeyError, TypeError):
            headquarters = None
        industry = self.get_industry(page_response.text, name, linkedin_id)
        try:
            logo = people_obj['logo']['image']['rootUrl'] + people_obj['logo']['image']['artifacts'][0][
                'fileIdentifyingUrlPathSegment']
        except TypeError:
            logo = None
        except KeyError:
            logo = None
        specialities = ', '.join(people_obj['specialities'])
        data_type = company_type.get('localizedName')
        total_employee_count = employees_on_linkedin
        location = self.get_all_locations(location_list)

        previous_data.source = company_name
        previous_data.name = name
        previous_data.linkedin_url = linkedin_url
        previous_data.company_address = company_address
        previous_data.source_url = source_url
        previous_data.company_size = company_size
        previous_data.description = description
        previous_data.employees_on_linkedin = employees_on_linkedin
        previous_data.follower_count = follower_count
        previous_data.founded = founded
        previous_data.headquarters = headquarters
        previous_data.industry = industry
        previous_data.linkedin_id = linkedin_id
        previous_data.others_id = others_id
        previous_data.logo = logo
        previous_data.specialities = specialities
        previous_data.total_employee_count = total_employee_count
        previous_data.data_type = data_type
        previous_data.location = location
        end = time.time()
        self.logger.info(f'dict_data - {end - start}')
        self.logger.info('END LONG BLOCK')
        return previous_data

    def get_person_function(self, function_dict, person_function):
        function_count = None
        try:
            function_count = next((sub for sub in function_dict if sub['displayValue'] == person_function), None)[
                'count']
        except KeyError:
            pass
        return function_count

    def _parse_people_html(self, page_response, previous_data):
        soup = BeautifulSoup(page_response.text, 'html.parser')
        # unavailable_page = self.detect_unavailable_page(page_response.text)
        # if unavailable_page:
        #     return_data = {'facet_current_company': None}
        #     return return_data
        code_tag = soup.find('code', text=re.compile('.*jobSearchPageUrl":"https.*'))
        return_list = []
        main_company = \
            re.findall('[0-9]+', re.search('jobSearchPageUrl":"(https.*?)"', code_tag.string).group(1))[1::2][0]
        return_list.append(main_company)
        try:
            other_companies = re.findall('[0-9]+', re.search(
                'affiliatedCompaniesWithEmployeesRollupResolutionResults":{"\*urn:li(.*?)}', code_tag.string).group(1))[
                              1::2]
            return_list = return_list + other_companies
        except AttributeError:
            return_list = return_list
        previous_data.facet_current_company = return_list
        return previous_data

    def _parse_ids_html(self, page_response, previous_data):
        page_response = page_response.json()

        valid_counts = [
            'Accounting',
            'Administrative',
            'ArtsAndDesign',
            'BusinessDevelopment',
            'CommunityAndSocialServices',
            'Consulting',
            'Education',
            'Engineering',
            'Entrepreneurship',
            'Finance',
            'HealthcareServices',
            'HumanResources',
            'InformationTechnology',
            'Legal',
            'Marketing',
            'MediaAndCommunication',
            'MilitaryAndProtectiveServices',
            'Operations',
            'ProductManagement',
            'ProgramAndProjectManagement',
            'Purchasing',
            'QualityAssurance',
            'RealEstate',
            'Research',
            'Sales',
            'Support'
        ]
        facet_dict = [i for i in page_response['metadata']['facets'] if
                      i['facetParameterName'] == 'facetCurrentFunction']
        try:
            html_counts = {''.join(list(map(str.title, i['displayValue'].split()))): i['count'] for i in
                           facet_dict[0]['facetValues']}
        except IndexError:
            html_counts = {}
        counts = {'distribution' + i: html_counts.get(i) for i in valid_counts}
        # distributions = DistributionData()
        previous_data.distributions = counts
        return previous_data

    def _parse_affilated_pages_json(self, page_response, previous_data):
        json_page = page_response.json()
        result = []

        if len(json_page['elements']) != 0:
            for card in json_page['elements'][0]['cards']:
                company_name = card['valueUnion']['entityCard']['entityLockupView']['title']['text']
                company_url = card['valueUnion']['entityCard']['entityLockupView']['navigationUrl']
                # company_type = card['valueUnion']['entityCard']['entityLockupView']['subtitle']['text']
                parentship = card['valueUnion']['entityCard']['secondarySubtitle']['text']
                result.append(AffiliatedPage(company_url, company_name, parentship))
        previous_data.affiliated_page = result
        return previous_data

    def _is_page_locked(self, page):
        if (page.status_code == 401 and page.reason == 'Unauthorized') or (page.status_code == 999) or page.history:
            return True
        page = page.text
        selector = Selector(text=page)
        elem = selector.xpath(
            '//a[contains(@href, "https://www.linkedin.com/legal/privacy-policy?trk=public_authwall_unknown_join-form-privacy-polic")]')
        return True if elem else False

    def get_page_selenium(self, link):
        self.driver.get(link)
        self.driver_requests += 1
        driver_html = self._get_driver_html()
        return driver_html

    def get_page_requests(self, link, query_strings, retry_count=5):
        session = self.session

        if self.account.proxy:
            if self.account.proxy_login and self.account.proxy_password:
                proxies_without_scheme = f'://{self.account.proxy_login}:{self.account.proxy_password}' \
                                        f'@{self.account.proxy.split("/")[-1]}'
            else:
                proxies_without_scheme = f'://{self.account.proxy.split("/")[-1]}'

            proxies = {
                'http': self.account.proxy.split(':')[0] + proxies_without_scheme,
                'https': self.account.proxy.split(':')[0] + proxies_without_scheme,
            }
        else:
            proxies = None

        csrf_token = session.cookies.get('JSESSIONID', '').replace('"', '')
        headers = dict(**HEADERS, **{'csrf-token': csrf_token,
                                     'x-restli-protocol-version': '2.0.0'})
        for _ in range(retry_count):
            try:
                response = session.get(
                    url=link,
                    headers=headers,
                    proxies=proxies,
                    params=query_strings,
                )
                response.encoding = 'utf-8'
                return response
            except requests.exceptions.ProxyError:
                self.logger.info(f'received proxy exception {_}')
        else:
            raise Exception('Cant parse', link, self.account.proxy)

    # def _refresh_request_session(self):
    #     self.session = requests.session()
    #     self.session_setuped = False

    # def _update_session_with_cookies(self, cookies):
    #     for cookie in cookies:
    #         self.session.cookies.set(**cookie)
    #     self.session_setuped = True

    # def _update_session_by_driver(self):
    #     for cookie in self.driver.get_cookies():
    #         self.session.cookies.set(cookie['name'], cookie['value'], domain=cookie['domain'])
    #     self.session_setuped = True

    def get_page(self, link, query_strings=None) -> Response:
        query_strings = {} if query_strings is None else query_strings
        retry = 1

        while retry <= self.retries:
            try:
                page_data = self.get_page_requests(link, query_strings)
                page_locked = self._is_page_locked(page_data)
            except BaseException:
                page_locked = True
                page_data = None

            retry += 1

            if page_locked and retry != self.retries:
                self.account, cookies = asyncio.get_event_loop().run_until_complete(
                        self.login_manager.login(
                            unrecognized_exception_account_id=self.account.id,
                        )
                )
                self.session.cookies.clear()

                for cookie in cookies:
                    self.session.cookies.set(**cookie)

                return self.get_page(
                    link=link,
                    query_strings=query_strings,
                )
            elif not page_locked:
                return page_data

    def parse_page(self, page_type, page, previous_data) -> (dict, list):
        # unavailable_page = self.detect_unavailable_page(page.content.decode("utf-8"))
        # if unavailable_page:
        #     page_data, extra_links, page = None, None, page.content.decode("utf-8")
        #     return page_data, extra_links, page
        parser_func = self.get_parser_functions.get(page_type)
        if parser_func is None:
            raise Exception('unsupported page type')
        page_data = parser_func(page, previous_data)
        extra_links = []
        if page_type == 'about':
            # mandatory, linkedin reject urlencoded strings
            extra_links = [
                Page('affilated_pages',
                     f'{self.domain}/voyager/api/voyagerOrganizationDashDiscoverCardGroups',
                     {
                         'decorationId': 'com.linkedin.voyager.dash.deco.organization.templates.FullCardGroup-33',
                         'organization': 'urn:li:fsd_company:' + page_data.linkedin_id,
                         'q': 'allCardsFromGroup',
                         'token': 'AFFILIATED_PAGES'}),
            ]

        elif page_type == 'people':
            people_params = {'count': 12,
                             'educationEndYear': 'List()',
                             'educationStartYear': 'List()',
                             'facetCurrentCompany': f"List({','.join(page_data.facet_current_company)})",
                             'facetCurrentFunction': 'List()',
                             'facetFieldOfStudy': 'List()',
                             'facetGeoRegion': 'List()',
                             'facetNetwork': 'List()',
                             'facetSchool': 'List()',
                             'facetSkillExplicit': 'List()',
                             'keywords': 'List()',
                             'maxFacetValues': 15,
                             'origin': 'organization',
                             'q': 'people',
                             'start': 0,
                             'supportedFacets': 'List(GEO_REGION,SCHOOL,CURRENT_COMPANY,CURRENT_FUNCTION,FIELD_OF_STUDY,SKILL_EXPLICIT,NETWORK)'}
            extra_links = [Page('voyager',
                                f'{self.domain}/voyager/api/search/hits' + '?' + '&'.join(
                                    ['='.join(list(map(str, i))) for i in people_params.items()]),
                                {}), ]

        return page_data, extra_links, page

    def save_proxies_df(self):
        self.proxies_df.to_excel('proxies.xlsx', index=False)

    def save_accounts_df(self):
        self.accounts_df.to_excel(f'accounts/accounts_{str(self.block_n)}.xlsx', index=False)

    def encode_string_db(self, string):
        return string.replace('.', '%%dot%%')

    def decode_string_db(self, string):
        return string.replace('%%dot%%', '.')

    def get_company_data(self, company_name):
        start = time.time()
        pages = [
            Page('about', f'{self.domain}/company/{company_name}/about', {}),
            Page('people', f'{self.domain}/company/{company_name}/people', {}),
        ]
        cache_pages = {}
        db_data = mongo_instance.get_data_by_company(company_name) or {}
        page_data = CompanyData(source=company_name)
        for page_type, page_link, query_strings in pages:
            decoded_link = self.encode_string_db(self._add_query_strings_to_link(page_link, query_strings))
            cached_data = db_data.get('result', {}).get('pages', {}).get(decoded_link)
            if cached_data and USE_CACHE:
                page_response = Response()
                page_response.url = page_link
                page_response._content = cached_data.encode()
            else:
                page_response = self.get_page(page_link, query_strings)
            cache_pages[self._add_query_strings_to_link(page_link, query_strings)] = page_response.text
            self.logger.info(f'start to parse {page_type} on {company_name}')
            page_data, additional_pages, page_response = self.parse_page(page_type, page_response, page_data)
            self.logger.info(f'finish to parse {page_type} on {company_name}')

            pages.extend(additional_pages)

        end = time.time()
        self.logger.info(f'company_data - {end - start}')
        return cache_pages, page_data

    def login(self):
        cookies = asyncio.get_event_loop().run_until_complete(self.login_manager.login())
        self.logger.info('cookies extracted')
        return cookies

    def parse_companies(self, retry_count=20):
        self.logger.info(f'COMPANIES TO SCRAPE: {len(self.company_names)}')
        parsed_companies = []
        for company in self.company_names:
            self.active_company = company
            company_data = {'error': False,
                            'exception': None,
                            'run_id': run_id,
                            'version': const.VERSION,
                            'run_timestamp': run_timestamp,
                            'pages': {},
                            'data': {}}
            self.logger.info(f'start to parse {company}')
            try:
                pages, page_data = self.get_company_data(company)
                pages = {self.encode_string_db(page): data for page, data in
                         pages.items()}
                company_data['pages'].update(pages)
                company_data['data'] = page_data.to_dict()
            except InvalidCompany:
                self.logger.info(f'{company} is invalid')
                company_data['error'] = True
                company_data['exception'] = 'InvalidCompany'
            except UnavailablePage:
                self.logger.info(f'{company} is unavailable')
                company_data['error'] = False
                company_data['data'] = CompanyData(linkedin_url='This LinkedIn Page isn’t available',
                                                   source=company,
                                                   company_address=CompanyAddress(company),
                                                   distributions={}).to_dict()
            except AccountsBanned:
                self.logger.info('All accounts is banned')
                exit()
            except Exception as e:
                self.logger.info(f'{company} errored with {e}')
                company_data['error'] = True
                company_data['exception'] = traceback.format_exc()
                if const.DEBUG:
                    raise
            parsed_companies.append(company_data)
            for _ in range(retry_count):
                try:
                    mongo_instance.upsert_company_data(company, company_data)
                    break
                except Exception as e:
                    time.sleep(5)
                    self.logger.exception(e)

            else:
                new_file = open("newfile.txt", mode="w", encoding="utf-8")
                new_file.write(f"{company}\n")
                pass

        return parsed_companies

    # def __del__(self):
    #     self.driver.close()

    @staticmethod
    def _add_query_strings_to_link(link, query_strings):
        return link + '?' + parse.urlencode(query_strings)


def get_company_names_from_file(file_path):
    print('start read excel')
    df = pd.read_csv(file_path)
    companies = df['LinkedInCompanyUrl'].str.strip('/').str.split('/').str[4].to_list()
    print('finish read excel')
    return companies


def get_to_skip_companies():
    skip_companies = mongo_instance.git_collection.find({'results.error': False}, {'_id': 1})
    return [company['_id'] for company in skip_companies]


def get_company_list(block_n):
    company_list = get_company_names_from_file('linkedin_urls_round_3.csv')
    # company_list = company_list[block_n*const.BLOCK_SIZE:(block_n+1)*const.BLOCK_SIZE]
    return company_list


def get_corrupted_companies():
    return ['freelance-self-employed_4']


def list_divide(
        list_data: list,
        number_of_pieces: int,
):
    return [
        [
            list_data[i] for i in range(len(list_data)) if (i % number_of_pieces) == r
        ]
        for r in range(number_of_pieces)
    ]


if __name__ == "__main__":
    # mongo_instance.clear_results()
    # exit()
    args = parser.parse_args()
    block_n = args.block_n
    company_list = get_company_list(0)
    unquoted_company_list = list()

    for company in company_list:
        unquoted_company_list.append(unquote(company))

    parsed_list = get_to_skip_companies()
    unquoted_company_list = [item for item in unquoted_company_list if item not in parsed_list]
    # unquoted_company_list = list_divide(unquoted_company_list, number_of_pieces=const.SCRAPERS_COUNT)[block_n]
    parser = LinkedinParser(unquoted_company_list)
    # print('start to parse', len(company_list), 'companies')
    res = parser.parse_companies()
    print("--- %s seconds ---" % (time.time() - start_time))
    del parser
