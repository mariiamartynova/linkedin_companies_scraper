import datetime
import os
import re

import openpyxl

from account_rotator.logger import create_logger
from account_rotator.rotator_db import Database, Account


class Loader:

    def __init__(self):
        self.logger = create_logger('[LOADER]')
        self.db = Database()

    def load_accounts_to_db(self):
        for account, proxy in zip(self.read_accounts(), self.read_proxies()):
            self.logger.info(f'STARTED CREATION ACCOUNT FROM THE DATA: {account} {proxy}')
            new_account = Account(
                email=account['email'],
                password=account['password'],
                email_password=account['email_password'],
                proxy=f'http://{proxy["ip_address"]}:{proxy["port"]}',
                proxy_login=proxy['login'],
                proxy_password=proxy['password'],
                in_use=False,
                is_unrecognized_exception=False,
                is_banned=False,
                created_at=datetime.datetime.now(),
            )
            self.db.add_account(account=new_account)

    def read_accounts(self):
        self.logger.info('READING ACCOUNTS FROM FILE...')
        accounts = list()
        dir_content = os.listdir('/'.join(str(__file__).split('/')[:-1]))

        for file in dir_content:

            if file.split('.')[-1] == 'xlsx':
                wb = openpyxl.load_workbook(file)
                ws = wb.active
                email_index = None
                password_index = None
                email_password_index = None

                for row_position, row in enumerate(ws.iter_rows(values_only=True)):

                    if row_position == 0:
                        email_index, password_index, email_password_index = self.get_indexes_from_main_row(row=row)
                        continue

                    accounts.append(
                        dict(
                            email=row[email_index],
                            password=row[password_index],
                            email_password=row[email_password_index],
                        )
                    )

                break
        else:
            raise Exception('The file with account not found')

        if not accounts:
            raise Exception(f'Not found account in the given file {file}')

        return accounts

    def read_proxies(self):
        self.logger.info('READING PROXIES FROM FILE...')

        with open('proxies_file') as proxies_file:
            file_content = proxies_file.read() \
                .replace(' ', '')
            proxies = list()

            try:
                proxies_port = re.findall(r'http/https(\d+)\n', file_content)[0]
            except Exception:
                raise Exception('Cannot read the proxies port from the file')

            try:
                proxies_login = re.findall(r'Логин:(.+)\n', file_content)[0]
            except Exception:
                raise Exception('Cannot read the proxies login from the file')

            try:
                proxies_password = re.findall(r'Пароль:(.+)\n', file_content)[0]
            except Exception:
                raise Exception('Cannot read the proxies password from the file')

            proxies_ips = re.findall(r'(\d+\.\d+\.\d+\.\d+)', file_content)

            if len(proxies_ips) == 0:
                raise Exception('Cannot find the proxies ip addresses in the file')

            for ip_address in proxies_ips:
                proxies.append(
                    dict(
                        ip_address=ip_address.strip(),
                        login=proxies_login if proxies_login.strip() else None,
                        password=proxies_password.strip() if proxies_password.strip() else None,
                        port=int(proxies_port.strip()),
                    )
                )

            return proxies

    @staticmethod
    def get_indexes_from_main_row(row: tuple) -> tuple:
        email_index = None
        password_index = None
        email_password_index = None

        for entry_index, entry in enumerate(row):
            if entry.strip().lower() == 'login':
                email_index = entry_index
            if entry.strip().lower() == 'password':
                password_index = entry_index
            if entry.strip().lower() == 'e-password':
                email_password_index = entry_index

        if not any([email_index, password_index, email_password_index]):
            raise Exception(
                'Cannot find desired columns in the accounts file. '
                'Valid columns: ["Login", "Password", "E-Password"] (register doesn\'t matter)'
            )

        return email_index, password_index, email_password_index


if __name__ == '__main__':
    loader = Loader()
    loader.load_accounts_to_db()
