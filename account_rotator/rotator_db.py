from sqlalchemy import Column, Integer, String, DateTime, Boolean, func, create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session

from account_rotator.logger import create_logger
from account_rotator.rotator_constants import DB_URL

Base = declarative_base()


class Account(Base):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    email = Column(
        String,
        nullable=False,
        unique=True,
    )
    password = Column(
        String,
        nullable=False,
    )
    email_password = Column(
        String,
        nullable=False,
    )
    proxy = Column(
        String,
        nullable=True,
    )
    proxy_login = Column(
        String,
        nullable=True,
    )
    proxy_password = Column(
        String,
        nullable=True,
    )
    in_use = Column(
        Boolean,
        nullable=False,
        default=False,
    )
    is_unrecognized_exception = Column(
        Boolean,
        nullable=False,
        default=False,
    )
    is_banned = Column(
        Boolean,
        nullable=False,
        default=False,
    )
    created_at = Column(
        DateTime,
        default=func.now(),
    )


class Database:

    def __init__(self, connection_url: str = DB_URL):
        self.logger = create_logger('[ROTATOR_DB]')
        self.engine = create_engine(connection_url)
        Base.metadata.create_all(self.engine)
        self.session = Session(bind=self.engine, autocommit=False)

    def get_free_account(self) -> Account:
        account = self.session.query(Account) \
            .with_for_update() \
            .filter(
            Account.is_banned == False,
            Account.in_use == False,
            Account.is_unrecognized_exception == False,
        ) \
            .first()

        if not account:
            account = self.session.query(Account) \
                .with_for_update() \
                .filter(
                Account.is_banned == False,
                Account.in_use == False,
            ) \
                .first()

        if account:
            account.in_use = True
            self.commit()

        return account

    def set_banned(
            self,
            account_id: int,
    ) -> None:
        try:
            self.session.query(Account) \
                .filter(
                Account.id == account_id,
            ).update(
                {
                    Account.is_banned: True,
                }
            )
            self.commit()
        except BaseException:
            self.session.rollback()

    def set_unrecognized_exception(
            self,
            account_id: int,
    ) -> None:
        try:
            account = self.session \
                .query(Account) \
                .filter(
                Account.id == account_id,
            ).one_or_none()

            if account.is_unrecognized_exception:
                account.is_banned = True
            else:
                account.is_unrecognized_exception = True
                account.in_use = False

            self.commit()
        except BaseException:
            self.session.rollback()

    def add_account(
            self,
            account: Account,
    ) -> None:
        self.session.add(account)
        success = self.commit()

        if success:
            self.logger.info('ACCOUNT ADDED')
        else:
            self.logger.info('ERROR! CURRENT EMAIL OR PROXIES ARE ALREADY IN DB!')

    def commit(self) -> bool:
        try:
            self.session.commit()
            return True
        except IntegrityError:
            self.session.rollback()
            return False
        except Exception as e:
            self.session.rollback()
            raise e
