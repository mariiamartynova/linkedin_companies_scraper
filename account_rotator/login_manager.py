import asyncio
import datetime
import email
import random
import re
import time
from asyncio import sleep
from typing import Tuple

import nest_asyncio
from pyppeteer import launch
from pyppeteer.page import ElementHandle
from pyppeteer_stealth import stealth

from account_rotator.logger import create_logger
from account_rotator.rotator_db import Database, Account
from proxy_imap import SocksIMAP4SSL

nest_asyncio.apply()


class LoginManager:
    _LOGIN_URL = 'https://www.linkedin.com/login/us'
    _LOAD_TIMEOUT = 120000
    _LOGIN_ACTION_TIMES = 20

    def __init__(self):
        self.logger = create_logger('[ACCOUNT_ROTATOR]')
        self.db = Database()
        self.browser = None
        self.page = None
        self.click_timestamp = None
        self.account = None
        self.click_datetime = None

    async def login(
            self,
            banned_account_id: int = None,
            unrecognized_exception_account_id: int = None,
    ) -> Tuple[Account, list]:
        try:
            if banned_account_id:
                self.db.set_banned(
                    account_id=banned_account_id,
                )
            elif unrecognized_exception_account_id:
                #  IF ACCOUNT IS ALREADY WITH unrecognized_exception = True WE WILL SET is_banned = True here
                self.db.set_unrecognized_exception(
                    account_id=unrecognized_exception_account_id,
                )

            self.account = self.db.get_free_account()

            if not self.account:
                self.logger.info('NO MORE ACCOUNTS! PLEASE ADD NEW ACCOUNTS TO THE DB!')
                await self.close_browser()
                await sleep(1200)
                return await self.login(
                    banned_account_id=self.account.id,
                )

            self.logger.info(
                f'Received new account. '
                f'Login: [{self.account.email}] Password: [{self.account.password}]. Proxy: [{self.account.proxy}]'
            )
            await self._configurate_the_page()
            await self._goto(
                url=self._LOGIN_URL,
            )
            await self._input_credentials()
            await self._click_confirmation_button()
            captcha = await self.resolve_captcha_if_exists()
            account_banned = await self._is_account_banned()

            if captcha or account_banned:
                self.logger.info(
                    f'ACCOUNT IS BANNED! '
                    f'Login: [{self.account.email}] Password: [{self.account.password}]'
                    f'Retrying with a new one...'
                )
                await self.close_browser()
                return await self.login(
                    banned_account_id=self.account.id,
                )

            await self._skip_popup_if_appears()
            await self._confirm_email_if_needed()
            await self._wait_for_login_completion()
            cookies = await self._extract_cookies()

            if self.valid_cookies(cookies=cookies):
                return self.account, cookies
            else:
                raise Exception('NOT ALL OF COOKIES WERE EXTRACTED. SEEMS LIKE THE AUTH IS FAILED')
        except BaseException as e:
            self.logger.info('UNRECOGNIZED EXCEPTION. RETRYING WITH NEW ACCOUNT...')
            await self.close_browser()
            return await self.login(
                unrecognized_exception_account_id=self.account.id,
            )

    async def resolve_captcha_if_exists(self):
        try:
            self.logger.info('CHECKING FOR CAPTCHA..')
            captcha = (
                await self.page.waitForXPath(
                    xpath='//h1[contains(text(), "Let\'s do a quick security check")]',
                    options={
                        'timeout': 5000,
                    },
                    timeout=5000,
                )
            )
            return captcha
        except Exception:
            return False

    async def _is_account_banned(self):
        try:
            self.logger.info('CHECKING FOR ACCOUNT BAN..')
            ban = (
                await self.page.waitForXPath(
                    xpath='//h1[contains(text(), "Your account has been restricted")]',
                    options={
                        'timeout': 5000,
                    },
                    timeout=5000,
                )
            )
            return ban
        except Exception:
            return False

    async def _confirm_email_if_needed(self) -> None:
        if 'checkpoint' in self.page.url and 'challenge' in self.page.url:
            self.logger.info('LINKEDIN REQUIRES EMAIL CONFIRMATION. BEGIN..')
            await sleep(3)
            await sleep(random.uniform(0, 2))
            code = await self._get_authentication_code()
            code_input_element = await self.page.waitForXPath(
                xpath='//input[contains(@class, "input--text") and @pattern="[0-9]*"]',
                options={
                    'timeout': 10000,
                },
                timeout=10000,
            )
            await code_input_element.type(code)
            confirm_button_element = await self.page.waitForXPath(
                xpath='//button[contains(@class, "form__submit")]',
                options={
                    'timeout': 10000,
                },
                timeout=10000,
            )
            await self._click_button(
                confirm_button_element
            )
            await sleep(5)

            try:
                skip_button_element = await self.page.waitForXPath(
                    xpath='//button[@class="secondary-action"]',
                    options={
                        'timeout': 10000,
                    },
                    timeout=10000,
                )
                await self._click_button(
                    button=skip_button_element,
                )
            except Exception:
                pass

            await sleep(5)
        else:
            self.logger.info('NO EMAIL CONFIRMATION NEEDED')

    async def _get_authentication_code(self) -> str:
        self.logger.info('TRYING TO RECEIVE THE AUTHENTICATION CODE FROM EMAIL..')
        host = "imap.mail.ru"
        host_port = 993

        if self.account.proxy:
            proxy_addr, proxy_port = self.account.proxy\
                .replace('https://', '')\
                .replace('http://', '')\
                .strip()\
                .split(':')
            proxy_type = 'https' if 'https://' in self.account.proxy else 'http'
            mail = SocksIMAP4SSL(
                host=host,
                port=host_port,
                proxy_addr=proxy_addr,
                proxy_port=int(proxy_port),
                proxy_type=proxy_type,
                username=self.account.proxy_login,
                password=self.account.proxy_password,
            )
        else:
            mail = SocksIMAP4SSL(
                host=host,
                port=host_port,
            )

        mail.login(
            self.account.email.strip(),
            self.account.email_password.strip(),
        )

        for _ in range(6):
            await sleep(10)
            mail.select()
            _, messages = mail.search(None, 'ALL')
            message_id_list = messages[0].split()

            for message_id in message_id_list[::-1]:
                _, message = mail.fetch(message_id, '(RFC822)')
                message_bytes = message[0][1]
                parsed_message = email.message_from_bytes(message_bytes)
                message_date_str = parsed_message['Date']
                message_from_str = parsed_message['From']
                message_timestamp = time.mktime(email.utils.parsedate(message_date_str))
                sender_name, sender_email = email.utils.parseaddr(message_from_str)
                click_datetime = self.click_datetime.replace(microsecond=0)
                message_datetime = datetime.datetime.fromtimestamp(message_timestamp).replace(microsecond=0)

                if message_datetime >= click_datetime - datetime.timedelta(seconds=30) \
                        and sender_email == 'security-noreply@linkedin.com':
                    message_text = str(parsed_message.get_payload()[0])
                    code = re.search(r'[0-9]{6}', message_text).group()
                    self.logger.info('THE CODE RECEIVED')
                    return code

        raise Exception('NO CONFIRMATION CODE')

    async def _configurate_the_page(self) -> None:
        self.logger.info('Begin the browser creation..')
        options = {
            'headless': True,
            'args': [
                '--no-sandbox',
                '--disable-dev-shm-usage',
                '--disable-setuid-sandbox',
                '--single-process',
                '--disable-dev-shm-usage',
                '--disable-gpu',
                '--no-zygote',
                "--use-gl=egl"
            ]
        }

        if self.account.proxy:
            options['args'].append(f'--proxy-server={self.account.proxy}')

        self.browser = await launch(
            options=options,
            timeout=120000,
        )
        self.page = await self.browser.newPage()

        if self.account.proxy_login and self.account.proxy_password:
            await self.page.authenticate(
                {
                    'username': f'{self.account.proxy_login}',
                    'password': f'{self.account.proxy_password}'
                }
            )

        await stealth(self.page)

    async def _goto(
            self,
            url: str,
            exceptions_count: int = 0
    ) -> None:
        self.logger.info(f'Open url: [{url}]..')
        try:
            await self.page.goto(
                url,
                timeout=self._LOAD_TIMEOUT,
            )
            self.logger.info(f'SUCCESSFULLY OPENED')
        except BaseException as e:
            if exceptions_count < 3:
                await self._goto(
                    url=url,
                    exceptions_count=exceptions_count + 1,
                )
            else:
                raise e

    async def _input_credentials(self) -> None:
        login_element = (
            await self.page.waitForXPath(
                xpath='//input[@id="username"]',
                options={
                    'timeout': 10000,
                },
                timeout=10000,
            )
        )
        await login_element.type(self.account.email)
        password_element = await self.page.waitForXPath(
            xpath='//input[@id="password"]',
            options={
                'timeout': 10000,
            },
            timeout=10000,
        )
        await password_element.type(self.account.password)

    async def _click_confirmation_button(self) -> None:
        self.logger.info('TRYING TO CONFIRM LOGIN..')
        try:
            button_element = await self.page.waitForXPath(
                xpath='//button[contains(text(), "Sign")]',
                options={
                    'timeout': 10000,
                },
                timeout=10000,
            )
            self.click_datetime = datetime.datetime.utcnow()
            await self._click_button(
                button=button_element,
            )
            await sleep(5)
        except BaseException:
            self.click_datetime = datetime.datetime.utcnow()

    async def _click_button(
            self,
            button: ElementHandle,
            reload: bool = True,
    ):
        await self.page.evaluate(
            '(element) => { element.click(); }',
            button,
        )

        if reload:
            await sleep(5)
            await self.page.reload()

        await sleep(5)

    async def _skip_popup_if_appears(self) -> None:
        self.logger.info('CHECKING FOR POP-UP WINDOW..')
        try:
            button_element = await self.page.waitForXPath(
                xpath='//button[contains(text(), "Skip")]',
                options={
                    'timeout': 5000,
                },
                timeout=5000,
            )
            await self._click_button(
                button=button_element,
            )
            await sleep(2)
        except BaseException:
            pass

    async def _wait_for_login_completion(self) -> None:
        self.logger.info('WAITING UNTIL THE LOGIN PAGE WILL BE LOADED..')

        for _ in range(self._LOGIN_ACTION_TIMES):
            url = self.page.url

            if url == 'https://www.linkedin.com/feed/':
                await sleep(5)
                return

            await sleep(1)
        else:
            await self.page.goto('https://www.linkedin.com/feed/')

        raise Exception('UNSUCCESSFUL LOGIN')

    async def _extract_cookies(self) -> list:
        self.logger.info('COOKIES EXTRACTION..')
        cookies = list()
        cookies_list = await self.page.cookies()
        for cookie in cookies_list:
            name = cookie['name']
            value = cookie['value']
            domain = cookie['domain']
            cookies.append({'name': name, 'value': value, 'domain': domain})
        return cookies

    async def close_browser(self):
        self.logger.info('CLOSING BROWSER..')
        try:
            await self.browser.close()
        except BaseException:
            pass

    @staticmethod
    def valid_cookies(
            cookies: list,
    ) -> bool:
        desired_cookies = ['JSESSIONID', 'bcookie', 'aam_uuid']
        cookies_keys = [cookie['name'] for cookie in cookies]
        return all(check_value in cookies_keys for check_value in desired_cookies)


if __name__ == '__main__':
    lm = LoginManager()
    asyncio.get_event_loop().run_until_complete(
        lm.login()
    )
