import logging

logging.basicConfig(
    format='%(asctime)s | %(name)s | %(levelname)s | %(message)s',
    level='INFO',
    datefmt='%Y-%m-%d%H:%M:%S'
)


def create_logger(
        name: str,
        log_level: str = 'INFO',
) -> logging.Logger:
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    return logger
