import mongodb_storage
import pandas as pd


def get_company_size(size_dict):
    company_size = str()
    if size_dict:
        start_count = size_dict['start']
        if 'start' in size_dict and 'end' in size_dict:
            company_size = f'{start_count}-{size_dict["end"]}'
        elif 'start' in size_dict and 'end' not in size_dict:
            if start_count != 0:
                company_size = f'{start_count}+'
            else:
                company_size = start_count
    else:
        company_size = None
    return company_size


def get_main_info(all_data):
    df_data = []
    affiliated_list = []
    n = 0
    for db_company in all_data:
        n += 1
        if n%100 == 0:
            print(n)
        company = list(filter(lambda res: res['error'] == False, db_company['results']))
        if not company:
            continue
        else:
            company = company[-1]['data']
        company_data = {}
        company_data['Source'] = company['source'] or 'https://www.linkedin.com/company/' + db_company['_id']
        company_data['LinkedInURL'] = company['linkedin_url']
        company_data['VisitWebsiteButton'] = company['source_url']
        company_data['Name'] = company['name']
        company_address = company['company_address']
        company_data['companyAddress'] = ', '.join(
            [company_address['city'] or '',
             company_address['postal_code'] or '', company_address['geographic_area'] or '',
             company_address['country'] or ''])
        company_data['companySize'] = get_company_size(company['company_size'])
        company_data['description'] = company['description']
        company_data.update(company['distributions'])
        company_data['employeesOnLinkedIn'] = company['employees_on_linkedin']
        company_data['followerCount'] = company['follower_count']
        company_data['founded'] = company['founded']
        company_data['headquarters'] = company['headquarters']
        company_data['industry'] = company['industry']
        company_data['LinkedinID'] = company['linkedin_id']
        try:
            company_data['OthersID'] = ', '.join(company['others_id'])
        except (TypeError, KeyError):
            company_data['OthersID'] = None
        company_data['logo'] = company['logo']
        company_data['specialties'] = company['specialities']
        company_data['totalEmployeeCount'] = company['total_employee_count']
        company_data['type'] = company['data_type']
        company_data['location'] = company['location']
        filtered_affiliated = company['affiliated_page']

        company_data['Has showcase?'] = 'y' if list(
            filter(lambda page: page['parentship'] == 'Showcase page', company['affiliated_page'])) else 'n'

        df_data.append(company_data)
        no_showcase_pages = list(filter(lambda page: page['parentship'] != 'Showcase page', company['affiliated_page']))
        affiliated_company_list = [{'LinkedinID': company_data['LinkedinID'],
                            'Source': company_data['Source'],
                            'Affiliated Page': page['company_url'],
                            'Type': page['parentship']} for page in no_showcase_pages]
        if company_data['LinkedInURL'] == 'This LinkedIn Page isn’t available':
            affiliated_company_list.append({'LinkedinID': company_data['LinkedinID'],
                            'Source': company_data['Source'],
                            'Affiliated Page': 'This LinkedIn Page isn’t available',
                            'Type': None})
        affiliated_list.extend(affiliated_company_list)

    affiliated = pd.DataFrame(affiliated_list)
    affiliated.to_csv('linkedin_affiliated_result_companies.csv', index=False)
    df = pd.DataFrame(df_data)
    df.to_csv('linkedin_main_result_companies.csv', index=False)


if __name__ == '__main__':
    mongo_instance = mongodb_storage.MongoDBStorage()
    mongo_instance.git_collection.find()

    total_docs = mongo_instance.git_collection.find().count()
    offset = 1000
    all_companies = list()
    cleaned_all_companies = list()
    batch_number = 0
    total_batches = int(total_docs / offset) + 1

    for i in range(0, total_docs + offset, offset):

        if i >= total_docs:
            print('list of companies is created')
            break

        batch_number += 1
        print(f'Start to handle batch ({batch_number} of {total_batches})..')
        batch_companies = list(
            mongo_instance.git_collection
                .find({'results.error': {'$eq': False}}, {'results.pages': False})
                .skip(i)
                .limit(offset)
        )
        all_companies.extend(batch_companies)

    for company in all_companies:
        if company not in cleaned_all_companies:
            cleaned_all_companies.append(company)

    get_main_info(cleaned_all_companies)
