from pathlib import Path

LOGIN = 'admin'
PASSWORD = '@H|oSRIiow1YrDWB'
IP = 'mongodb1.accelerator-introlab.ml'
DB_NAME = 'LinkedIn_scraper'
COLLECTION_NAME = 'LinkedIn_scraper'
VERSION = 1.5
BLOCK_SIZE = 11000
DEBUG = False
USE_CACHE = False
SKIP_CACHE = True
BASE_DIR = Path(__file__).resolve().parent
SCRAPERS_COUNT = 5
