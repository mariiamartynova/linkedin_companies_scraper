from dataclasses import dataclass
from enum import Enum
from collections import namedtuple
from enum import Enum
from typing import List


class ActionType(Enum):
    REQUEST = 1
    SELENIUM = 2
    REQUEST_REFRESH = 3
    SELENIUM_REFRESH = 4


@dataclass
class Action:
    action_type: ActionType
    action_time: float
    result: bool = None


@dataclass(eq=True, frozen=True, repr=True)
class LoginData:
    proxy: str
    proxy_login: str
    proxy_password: str
    linkedin_login: str
    linkedin_password: str
    email_login: str
    email_password: str


@dataclass
class CacheConfig:
    SKIP = 1
    USE = 1



@dataclass
class CompanyAddress(dict):
    company_name: str
    country: str = None
    geographic_area: str = None
    city: str = None
    postal_code: str = None
    description: str = None
    street_address_opt_out: bool = None
    headquarter: bool = None
    line1: str = None


@dataclass
class DistributionData(dict):
    company_name: str
    accounting: str = None
    administrative: str = None
    artsanddesign: str = None
    businessdevelopment: str = None
    communityandsocialservices: str = None
    consulting: str = None
    education: str = None
    engineering: str = None
    entrepreneurship: str = None
    finance: str = None
    healthcareservices: str = None
    humanresources: str = None
    informationtechnology: str = None
    legal: str = None
    marketing: str = None
    mediaandcommunication: str = None
    militaryandprotectiveservices: str = None
    operations: str = None
    productmanagement: str = None
    programandprojectmanagement: str = None
    purchasing: str = None
    qualityassurance: str = None
    realestate: str = None
    research: str = None
    sales: str = None
    support: str = None


@dataclass
class CompanySize(dict):
    company_name: str
    start: int = None
    end: int = None


@dataclass
class AffiliatedPage:
    company_url: str
    company_name: str
    parentship: str


@dataclass
class CompanyData(dict):
    source: str
    name: str = None
    linkedin_url: str = None
    company_address: CompanyAddress = None
    distributions: DistributionData = None
    affiliated_page: List[AffiliatedPage] = ()
    company_size: CompanySize = None
    source_url: str = None
    description: str = None
    employees_on_linkedin: int = None
    follower_count: int = None
    founded: int = None
    headquarters: str = None
    industry: str = None
    linkedin_id: str = None
    others_id: list = None
    logo: str = None
    specialities: str = None
    total_employee_count: int = None
    data_type: str = None
    location: str = None
    facet_current_company: list = ()

    def to_dict(self):
        self_object = self.__dict__
        self_object['affiliated_page'] = [i.__dict__ for i in self_object['affiliated_page']]
        # if self_object['distributions'] is None:
        #     self_object['distributions'] = self_object['distributions'].__dict__
        if self_object['company_size'] is not None:
            self_object['company_size'] = self_object['company_size'].__dict__
        if self_object['company_address'] is not None:
            self_object['company_address'] = self_object['company_address'].__dict__
        return self_object


Page = namedtuple('Page', ['page_type', 'link', 'query_strings'])
