import asyncio
import imaplib
import logging
import re
from asyncio import sleep

import nest_asyncio
import pyppeteer.connection
from pyppeteer import launch
from pyppeteer_stealth import stealth
import email
import datetime
import time
nest_asyncio.apply()

# format_message = u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s'
# logging.basicConfig(format=format_message, level=logging.INFO)

class LoginManager:
    _LOGIN_URL = 'https://www.linkedin.com/login/us'
    _LOAD_TIMEOUT = 120000
    _LOGIN_ACTION_TIME_S = 20
    click_timestamp = None

    def __init__(self):
        self.browser = None
        self.page = None
        self.click_timestamp = None
        #self.logger = logging.getLogger(__name__)

    async def login(
            self,
            login: str,
            password: str,
            email_login: str,
            email_password: str,
            proxy: str = None,
            proxy_login: str = None,
            proxy_password: str = None,
    ) -> dict:
        #self.logger.info('STEP LOGIN')
        await self._configurate_the_page(
            proxy=proxy,
            proxy_login=proxy_login,
            proxy_password=proxy_password,
        )
        await self._check_page_hiding()
        await self._goto(url=self._LOGIN_URL)
        await self._input_credentials(login, password)
        await self._click_confirmation_button()
        account_banned = await self._is_account_banned()
        if account_banned:
            return None
        await self._skip_phone_confirmation_if_appears()
        await self._confirm_email_if_needed(email_login, email_password)
        await self._wait_for_login_completion()
        return await self._extract_cookies()

    async def _is_account_banned(self):
        ban_h1 = (await self.page.xpath('//h1[contains(text(), "Your account has been restricted")]'))
        return True if ban_h1 else False

    async def _confirm_email_if_needed(
            self,
            email_login: str,
            email_password: str,
            retry_count: int = 0,
    ) -> None:
        if 'checkpoint' in self.page.url and 'challenge' in self.page.url:  # todo add email confirmation checker
            print('_confirm_email_if_needed', 'found checkpoint')
            try:
                await sleep(3)
                code = await self._get_authentication_code(
                    email_login=email_login,
                    email_password=email_password,
                )
                code_input_element = (
                    await self.page.xpath('//input[contains(@class, "input--text") and @pattern="[0-9]*"]')
                )[0]
                await code_input_element.type(code)
                confirm_button_element = (
                    await self.page.xpath('//button[contains(@class, "form__submit")]')
                )[0]
                await confirm_button_element.click()
                await sleep(10)
                #self.logger.info('STEP form__submit')

                try:
                    skip_button_element = (
                        await self.page.xpath('//button[@class="secondary-action"]')
                    )[0]
                    await skip_button_element.click()
                except Exception:
                    pass
                #self.logger.info('STEP button click')

                await sleep(10)

            except Exception as e:
                print('_confirm_email_if_needed', 'error', retry_count)
                if retry_count < 3:
                    await self._goto('https://www.linkedin.com/feed/')
                    #self.logger.info('STEP confirm_email_if_needed')
                    return await self._confirm_email_if_needed(
                        email_login=email_login,
                        email_password=email_password,
                        retry_count=retry_count + 1,
                    )


                else:
                    #self.logger.info('STEP confirm_email exception')
                    raise e

            #self.logger.info('STEP confirm_email')

    async def _get_authentication_code(
            self,
            email_login: str,
            email_password: str
    ) -> str:
        wait_time = 60*3
        print('_get_authentication_code', 'start')
        mail = imaplib.IMAP4_SSL("imap.rambler.ru", 993)
        mail.login(
            email_login,
            email_password,
        )
        while datetime.datetime.utcnow().timestamp() < self.click_datetime.timestamp() + wait_time:
            mail.select()
            _, messages = mail.search(None, 'ALL')
            message_id_list = messages[0].split()
            for message_id in message_id_list[::-1]:
                _, message = mail.fetch(message_id, '(RFC822)')
                message_bytes = message[0][1]
                parsed_message = email.message_from_bytes(message_bytes)
                message_date_str = parsed_message['Date']
                message_from_str = parsed_message['From']
                message_timestamp = time.mktime(email.utils.parsedate(message_date_str))
                sender_name, sender_email = email.utils.parseaddr(message_from_str)
                click_datetime = self.click_datetime.replace(microsecond=0)
                message_datetime = datetime.datetime.fromtimestamp(message_timestamp).replace(microsecond=0)

                if message_datetime >= click_datetime and sender_email == 'security-noreply@linkedin.com':
                    message_text = str(parsed_message.get_payload()[0])
                    code = re.search(r'[0-9]{6}', message_text).group()
                    return code
                elif message_datetime < click_datetime:
                    break
        raise Exception('no code')

    async def _configurate_the_page(
            self,
            proxy: str,
            proxy_login: str = None,
            proxy_password: str = None,
    ) -> None:
        print('_configurate_the_page', 'start')
        loop = asyncio.get_event_loop()
        self.browser = loop.run_until_complete(
            launch(
                headless=True,  # todo: set in True after tests
                timeout=120000,
                args=[
                    f'--proxy-server={proxy}',
                    '--no-sandbox',
                    '--disable-dev-shm-usage',
                    '--disable-setuid-sandbox',
                    '--single-process',
                    '--disable-dev-shm-usage',
                    '--disable-gpu',
                    '--no-zygote'
                ]
            )
        )
        self.page = loop.run_until_complete(self.browser.newPage())

        if proxy_login and proxy_password:
            print('_configurate_the_page', 'authenticate')
            await self.page.authenticate({'username': f'{proxy_login}', 'password': f'{proxy_password}'})

        loop.run_until_complete(stealth(self.page))

    async def _goto(
            self,
            url: str,
            exceptions_count: int = 0
    ) -> None:
        print('_configurate_the_page', 'goto', 'url')
        try:
            await self.page.goto(
                url,
                timeout=self._LOAD_TIMEOUT,
            )
        except Exception:
            print('_configurate_the_page', 'error', exceptions_count)
            if exceptions_count < 3:
                await self._goto(
                    url=url,
                    exceptions_count=exceptions_count + 1,
                )

    async def _check_page_hiding(self) -> None:
        try:
            await self._goto(
                url='https://bot.sannysoft.com',
            )
            await sleep(2)
        except Exception:
            pass

    async def _input_credentials(
            self,
            login: str,
            password: str,
    ) -> None:
        login_element = (await self.page.xpath('//input[@id="username"]'))[0]
        await login_element.type(login)
        password_element = (await self.page.xpath('//input[@id="password"]'))[0]
        await password_element.type(password)

    async def _click_confirmation_button(self) -> None:
        print('_click_confirmation_button', 'start')
        button_element = (await self.page.xpath('//button[contains(text(), "Sign")]'))[0]
        self.click_datetime = datetime.datetime.utcnow()
        await button_element.click()
        await sleep(5)

    async def _skip_phone_confirmation_if_appears(self) -> None:
        print('_skip_phone_confirmation_if_appears', 'start')
        try:
            button_element = (await self.page.xpath('//button[contains(text(), "Skip")]'))[0]
            await button_element.click()
            await sleep(2)
        except Exception:
            pass

    async def _wait_for_login_completion(self) -> None:
        print('_wait_for_login_completion', 'start')

        for _ in range(self._LOGIN_ACTION_TIME_S):
            print('_wait_for_login_completion', 'run', _)

            url = self.page.url

            if url == 'https://www.linkedin.com/feed/':
                break

            await sleep(1)
        else:
            await self.page.goto('https://www.linkedin.com/feed/')

        await sleep(5)

    async def _extract_cookies(self) -> list:
        cookies = list()
        cookies_list = await self.page.cookies()
        for cookie in cookies_list:
            name = cookie['name']
            value = cookie['value']
            domain = cookie['domain']
            cookies.append({'name': name, 'value': value, 'domain': domain})
        return cookies

    async def close(self):
        print('close', 'start')
        if self.browser:
            await self.browser.close()
        self.__init__()
        print('b')


def patch_pyppeteer():
    original_method = pyppeteer.connection.websockets.client.connect

    def new_method(*args, **kwargs):
        kwargs['ping_interval'] = None
        kwargs['ping_timeout'] = None
        return original_method(*args, **kwargs)

    pyppeteer.connection.websockets.client.connect = new_method


# patch_pyppeteer()

if __name__ == '__main__':
    lm = LoginManager()
    asyncio.get_event_loop().run_until_complete(
        lm.login(
            login='macdonaldirene16@rambler.ru',
            password='w9MjkKpxe',
            email_login='macdonaldirene16@rambler.ru',
            email_password='PfXowrujnr0w',
            proxy='http://45.145.57.209:14454',
            proxy_login='WbS5K3',
            proxy_password='9jP3mJ',
        )
    )
